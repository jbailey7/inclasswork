# File Name: Makefile
# Author: Matthew Morrison
# E-mail: matt.morrison@nd.edu 
#
# This file contains the Makefile for compiling the code in the 
# Recursion Lecture Notes 
# First, identify the compiler to be used.
C++ = g++

# Next, add the compiler flags, which we call CFLAGS
# -g    			adds debugging information to the executable file
# -std::gnu++11 	GNU C++ compiler, g++, provides extensions to the C++ language
#					For more info on gnu++11 https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Extensions.html
# -Wpedantic		Issue all the warnings demanded by strict ISO C and ISO C++	
CFLAGS = -g -std=gnu++11 -Wpedantic

########################### Executables ###############################

# Factorial
FACTORIAL = fact
# First, create a message to send to the user
factMsg = $(info ********Compiling Factorial Example ***********)
# Next, indicate the compilation commands
factCmp = $(C++) $(CFLAGS) $(FACTORIAL).cpp -o $(FACTORIAL)

# Command: make fact 
fact: $(FACTORIAL).cpp
	$(factMsg) 
	$(factCmp)

# Factorial with Supplemental
FACTORIALNEW = factNew
# First, create a message to send to the user
factNewMsg = $(info ********Compiling Factorial Example with Supplemental ***********)
# Next, indicate the compilation commands
factNewCmp = $(C++) $(CFLAGS) $(FACTORIALNEW).cpp -o $(FACTORIALNEW)

# Command: make fact 
factNew: $(FACTORIALNEW).cpp
	$(factNewMsg) 
	$(factNewCmp)


# Exponential
EXPO = exp
# First, create a message to send to the user
expoMsg = $(info ********Compiling Exponential Example ***********)
# Next, indicate the compilation commands
expCmp = $(C++) $(CFLAGS) $(EXPO).cpp -o $(EXPO)

# Command: make exp 
# output: ********COMPILING STRUCT VERSION************
# output: g++ -g -std=gnu++11 -Wpedantic testDateStr.cpp -o testDateStr
exp: $(EXPO).cpp
	$(expoMsg) 
	$(expCmp)

# English Ruler Example
ENGLISH = English
# First, create a message to send to the user
englishMsg = $(info ********Compiling English Ruler Example ***********)
# Next, indicate the compilation commands
englishCmp = $(C++) $(CFLAGS) $(ENGLISH).cpp -o $(ENGLISH)

# Command: make engl 
engl: $(ENGLISH).cpp
	$(englishMsg) 
	$(englishCmp)

# Factorial Tail Example
FACT_T = factTail
# First, create a message to send to the user
factTMsg = $(info ********Compiling Factorial Tail Example ***********)
# Next, indicate the compilation commands
factTCmp = $(C++) $(CFLAGS) $(FACT_T).cpp -o $(FACT_T)

# Command: make engl 
factTailCmp: $(FACT_T).cpp
	$(factTMsg) 
	$(factTCmp)


# MergeSort Example
MERGE = mergeSort
# First, create a message to send to the user
mergeMsg = $(info ********Compiling Factorial Tail Example ***********)
# Next, indicate the compilation commands
mergeCmp = $(C++) $(CFLAGS) $(MERGE).cpp -o $(MERGE)

# Command: make engl 
mergeSort: $(MERGE).cpp
	$(mergeMsg) 
	$(mergeCmp)
	

# Recursive SLL Delete kth element Example
kthElem = kthElem
# First, create a message to send to the user
kthMsg = $(info ********Compiling Delete kth element in a SLL ***********)
# Next, indicate the compilation commands
kthCmp = $(C++) $(CFLAGS) $(kthElem).cpp -o $(kthElem)

# Command: make engl 
kth: $(kthElem).cpp
	$(kthMsg) 
	$(kthCmp)

######################### Command: make all ###############################

# First, create a message to the user that this is for both versions
allMsg = $(info ********COMPILING All Recursion Programs ************)

# Command: make all

all: 
	$(allMsg)
	$(factCmp) 
	$(factNewCmp)
	$(expCmp)
	$(englishCmp)
	$(factTailCmp)
	$(mergeCmp)
	$(kthCmp)


######################### Command: make clean ###############################
cleanMsg = $(info ********make clean************)
cleanAll = $(RM) -rf $(FACTORIAL) $(FACTORIALNEW) $(EXPO) $(FACT_T) $(ENGLISH) $(MERGE) $(kth) *.o *.out

clean: 
	$(cleanMsg)
	$(cleanAll)